<?php
// Client
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;


require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';

require_once('./gsheets/sheets.php');

   require __DIR__ . '/vendor/autoload.php';

$client = new \Google_Client();
$client->setApplicationName('Google Sheets with Primo');
$client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
$client->setAccessType('offline');
$client->setAuthConfig(__DIR__ . '/credentials.json');
//$mail->AddEmbeddedImage('./assets/growgroups.png', 'logo_gg');


$service = new Google_Service_Sheets($client);
$spreadsheetId = '1b8jfQGHQ7ZbBURljKxWB9AqpQBGPkUqWdbEG1h3w4nk';

date_default_timezone_set('America/New_York');
 $date = date('m-d-Y') ;

//Server Creds
$servername = "localhost";
$serverUserName = "technology1u_admin";
$serverPassword = "members2CONNECT!";
$dbname = "technology1u_1u_applications";
$conn = new mysqli($servername, $serverUserName, $serverPassword, $dbname);

//Basic Information
 $fname = htmlspecialchars($_POST['fname']);
 $lname = htmlspecialchars($_POST['lname']);
 $email = htmlspecialchars($_POST['email']);
 $cell = htmlspecialchars($_POST['cell']);
 $interests = ' ';
 $sheetData = ' ';
 $interestHTML = ' ';
 $sheetsID ='1b8jfQGHQ7ZbBURljKxWB9AqpQBGPkUqWdbEG1h3w4nk';

$mail = new PHPMailer(true);
                
    $mail->isSMTP();                                           
    $mail->Host       = '1usda.technology';                     
    $mail->SMTPAuth   = true;                                   
    $mail->Username   = 'no-reply@1usda.technology';                    
    $mail->Password   = 'Jesus777!';                             
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            
    $mail->Port       = 465;    
    
    $mail->setFrom('no-reply@1usda.technology', '1U Automation');

    $mail->addAddress('records@1usda.org');               
   
    // 1Body for All Messages
    $mail->isHTML(true);                                     
    $mail->Body    = "Name: $fname $lname <br>Cell: $cell <br>Email: $email";
    $mail->AltBody = "Name: $fname $lname\nCell: $cell\nEmail: $email";

	if (isset($_POST['g-recaptcha-response'])) {
		$secret_key = '6LdDIggpAAAAAP-omLpfd5VRAPRjSq7nhyaDEy7U';
		$url = 'https://www.google.com/recaptcha/api/siteverify?secret='.$secret_key.'&response='.$_POST['g-recaptcha-response'];
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HEADER, false);
		$data = curl_exec($curl);
		curl_close($curl);
		$response = json_decode($data);
	 
		if($response->success) {
			if (isset($_POST['growgroups'])) 
			{
				foreach($_POST['growgroups'] as $growgroups)
				{
					
					if ($growgroups === 'cry')
					{
						$mail->Subject = "Interested in Grow Group Cry Like A Men";
						$mail->addAddress("sfullard2101@gmail.com");
						$mail->send();
						$mail->clearAddresses();
						$interests .= "Cry Like A Men\n ";
						$sheetData .= 'cry  ';
					} 
					if ($growgroups === 'BB')
					{
						$mail->Subject = "Interested in Grow Group Books of the Bible";
						$mail->addAddress("stacyy.jordan@gmail.com");
						$mail->send();
						$mail->clearAddresses();
						$interests .= "Books of the Bible\n ";
						$sheetData .= 'BB  ';
					} 
					if ($growgroups === 'BY')
					{
						$mail->Subject = "Interested in Grow Group Be You";
						$mail->addAddress("elder.white@1usda.org");
						$mail->send();
						$mail->clearAddresses();
						$interests .= "Be You\n ";
						$sheetData .= 'BY  ';
					} 
					if ($growgroups === 'FC')
					{
						$mail->Subject = "Interested in Grow Group Financial Contentment";
						$mail->addAddress("Ker11kah@gmail.com");
						$mail->send();
						$mail->clearAddresses();
						$interests .= "Financial Contentment\n ";
						$sheetData .= 'FC  ';
					} 
					if ($growgroups === 'GC')
					{
						$mail->Subject = "Interested in Grow Group Growing in Christ";
						$mail->addAddress("pastor@1usda.org");
						$mail->send();
						$mail->clearAddresses();
						$mail->addAddress("elder.woods@1usda.org");
						$mail->send();
						$mail->clearAddresses();
						$interests .= "Growing in Christ\n ";
						$sheetData .= 'GC  ';
					} 
					if ($growgroups === 'CB')
					{
						$mail->Subject = "Interested in Grow Group Crossing the Bridge";
						$mail->addAddress('Gaba94@yahoo.com');
						$mail->send();
						$mail->clearAddresses();
						$interests .= "Crossing the Bridge\n ";
						$sheetData .= 'CB  ';
						$interestHTML .= ' ';
						
					}
					if ($growgroups === 'PFFF')
					{
						$mail->Subject = "Interested in Grow Group Wedensay Power Thru with Food, Faith, and Fellowship";
						$mail->addAddress('elder.white@1usda.org');
						$mail->send();
						$mail->clearAddresses();
						$interests .= "Wednesday Night Bible Study\n ";
						$sheetData .= 'PFFF  ';
						$interestHTML .= ' ';
					} 
					if ($growgroups === 'FF')
					{
						$mail->Subject = "Interested in Grow Group Fervent Foodies";
						$mail->addAddress('greendayna1@gmail.com');
						$mail->send();
						$mail->addAddress('linedwards2@gmail.com');
						$mail->send();
						$mail->clearAddresses();
						$interests .= "Fervent Foodies\n ";
						$sheetData .= 'FF  ';
						$interestHTML .= ' ';
					} 
					if ($growgroups === 'BY')
					{
						$mail->Subject = "Interested in Grow Group Becoming You ";
						$mail->addAddress('elder.white@1usda.org');
						$mail->send();
						$mail->clearAddresses();
						$interests .= "Becoming You \n ";
						$sheetData .= 'BY  ';
						$interestHTML .= ' ';
					} 
					if ($growgroups === 'KW')
					{
						$mail->Subject = "Kingdom Women";
						$mail->addAddress("S_denny_3@hotmail.com");
						$mail->send();
						$mail->clearAddresses();
						$interests .= "Kingdom Women\n ";
						$sheetData .= 'KW  ';
						$interestHTML .= ' ';
					}  
					if ($growgroups === 'CK')
					{
						$mail->Subject = "Interested in Grow Group Cooking with Jesus";
						$mail->addAddress("Gaba94@yahoo.com");
						$mail->send();
						$mail->clearAddresses();
						$interests .= "Cooking with Jesus\n ";
						$sheetData .= 'CK  ';
						$interestHTML .= ' ';
					}  
					if ($growgroups === 'BLB')
					{
						$mail->Subject = "Interested in Grow Group Blended and Blessed ";
						$mail->addAddress("beniandthejess@gmail.com");
						$mail->send();
						$mail->clearAddresses();
						$interests .= "Blended and Blessed \n ";
						$sheetData .= 'BLB  ';
						$interestHTML .= ' ';
					}  
				}
						$mail->clearAddresses();
						$mail->Subject = "$fname Grow Group Interest";
						$mail->addAddress("growgroups@1usda.org");
						$mail->Body    = "Name: $fname $lname <br>Cell: $cell <br>Email: $email<br>Interests: $interests";
						$mail->AltBody = "Name: $fname $lname\nCell: $cell\nEmail: $email\nInterest: $interests";
						$mail->send();
						
						$mail->clearAddresses();
						$mail->Subject = "$fname Your Grow Group Confirmation";
						$mail->addAddress("$email");
						$mail->Body    = "<div style=\'margin: auto;display: block;  margin-left: auto;  margin-right: auto; width: 30%;\'><h1 style=\"color: green;\">What you have submitted</h1> <br><b>Name:</b> $fname $lname <br><b>Cell:</b> $cell <br><b>Email:</b> $email<br><b>Interests:</b> $interests<br><br>Thank You for your interest! Someone will contact you soon.</div>";
						$mail->AltBody = "What you have submitted\nYour Name: $fname $lname\nCell: $cell\nEmail: $email\nInterest: $interests";
						$mail->send();
						
							$sql = "INSERT INTO growgroup (firstName, lastName, cell,     email,    groups,      dates )
                                                   VALUES ('$fname', '$lname', '$cell', '$email', '$sheetData', '$date' )";
                                      $conn->query($sql);

						
			}   
			header("Location: https://firstuniversitysda.org/thanks/");
		} else {
			echo 'Verification failed';
		}
	}



                

?>
